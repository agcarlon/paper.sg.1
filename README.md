This is a paper about how to use the stochastic gradient to optimize Bayesian design of experiments.

Methods used for optimization:

-Stochastic gradient

-Nesterov accelerated gradient

-Candes restart technique for the acceleration

Methods used to deal with uncertainties:

-Monte Carlo

-Laplace

Numerical examples:

-Stochastic optimization quadratic problem

-convex model problem

-Timoshenko beam strain gauge placement

-EIT problem
