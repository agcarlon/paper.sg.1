Ms. Ref. No.:  CMAME-D-18-00720R1
Title: Nesterov-aided Stochastic Gradient Methods using Laplace Approximation for Bayesian Design Optimization
Ben M Dia; Luis F Espath; Rafael H Lopez; Raul F Tempone
Computer Methods in Applied Mechanics and Engineering

Dear Ms. André Gustavo Carlon,

The reviewers have now commented on your paper and have advised against publication in its present form. Their reconsideration will require extensive revision, as described in their comments appended below. If you decide to submit a revision, please accompany it with a detailed rejoinder explaining how you responded to each point raised by the reviewers.

Please submit your revision by Feb 24 2019 12:00AM, in accordance with the following procedure:

1. Go to: https://ees.elsevier.com/cmame/

2. Enter your login details
Your username is: ******

If you need to retrieve password details, please go to: http://ees.elsevier.com/CMAME/automail_query.asp

3. Click [Author Login]
This takes you to the Author Main Menu.

4. Click [Submissions Needing Revision]

Please note that this journal offers a new, free service called AudioSlides: brief, webcast-style presentations that are shown next to published articles on ScienceDirect (see also http://www.elsevier.com/audioslides). If your paper is accepted for publication, you will automatically receive an invitation to create an AudioSlides presentation.

Include interactive data visualizations in your publication and let your readers interact and engage more closely with your research. Follow the instructions here: https://www.elsevier.com/authors/author-services/data-visualization to find out about available data visualization options and how to include them with your article.

MethodsX file (optional)
We invite you to submit a method article alongside your research article. This is an opportunity to get full credit for the time and money you have spent on developing research methods, and to increase the visibility and impact of your work. If your research article is accepted, your method article will be automatically transferred over to the open access journal, MethodsX, where it will be editorially reviewed and published as a separate method article upon acceptance. Both articles will be linked on ScienceDirect. Please use the MethodsX template available here when preparing your article: https://www.elsevier.com/MethodsX-template. Open access fees apply.


Yours sincerely,

J. Tinsley Oden, PhD
Editor
Computer Methods in Applied Mechanics and Engineering

*********************************************************


Ms. Ref. No.:  CMAME-D-18-00720R1
Title: Nesterov-aided Stochastic Gradient Methods using Laplace Approximation for Bayesian Design Optimization
Author(s): Ben M Dia; Luis F Espath; Rafael H Lopez; Raul F Tempone
Computer Methods in Applied Mechanics and Engineering

Reviewers' comments:

Reviewer #1: Revisions required. The authors have made substantial revisions to their manuscript and addressed most of my earlier comments. I have a few remaining comments:

- Responding to my earlier comment #7, on the fact that all estimators of expected information gain considered here are *biased* at finite M, the authors propose to evaluate the "full gradient of the DCMIS estimator" at the optimum produced by another (cheaper) method, and to use smallness of the gradient norm as an indicator that the biased optimum is not far from the true optimum. This idea of course rests on the DCMIS estimator itself not being strongly biased, and on the bias not varying sharply as a function of the design parameter \xi. Also, if the objective is shallow in general, the optimal designs might still be rather far from each other. Overall, the statement seems rather vague. Given its importance, can it be formalized and made more precise? In other words, can you explain under what conditions (on the Bayesian model) this test is meaningful?

- The authors now state that one of the main contributions of this paper is to derive the gradient of the double-loop Monte Carlo estimator of the expected Shannon information gain. This result is indeed useful, but reference [3] seems to provide a similar derivation (for DLMC, not MCLA).


One final comment is that the English language usage in the paper is still poor and occasionally hard to understand. The paper would benefit from careful copy-editing by a fluent native speaker.

Reviewer #2
Download PDF Comments from link on the Author's submission page.
