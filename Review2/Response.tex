\documentclass[3p, 12pt, sort&compress]{elsarticle}
\usepackage{float} % To re-style the 'figure' float
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{graphics}
\usepackage{amsthm}
\usepackage{latexsym,amsfonts}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{xcolor}

\newcommand\myeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\scriptsize def}}}{=}}}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\blue}[1]{{ #1}}

\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\bs}[1]{\boldsymbol{#1}}

\pdfsuppresswarningpagegroup=1
% margin notes
\begin{document}

\begin{frontmatter}



 \title{Response to reviewers\\ {\small Nesterov-aided Stochastic Gradient Methods using Laplace Approximation for Bayesian Design Optimization}}


 % Group authors per affiliation:
 \author[ufsc]{AG Carlon\corref{mycorrespondingauthor}}
 \cortext[mycorrespondingauthor]{Corresponding author.\\
  \quad E-mail addresses: a.g.carlon@posgrad.ufsc.br (AG Carlon),
  ben.dia@kfupm.edu.sa (BM Dia),
  espath@gmail.com (LFR Espath),
  rafaelholdorf@gmail.com (RH Lopez),
 raul.tempone@kaust.edu.sa (R Tempone)}
 \address[ufsc]{Federal University of Santa Catarina (UFSC), Department of Civil Engineering, Rua Jo\~ao Pio Duarte da Silva, Florian\'opolis, SC, 88040-970, Brazil}

 \author[kfupm]{BM Dia}
 \address[kfupm]{ King Fahd University of Petroleum and Minerals (KFUPM), College of Petroleum Engineering and Geosciences, Center for Integrative Petroleum Research (CIPR), Dhahran 31261, Saudi Arabia}

 \author[kaust]{LFR Espath}

 \author[ufsc]{RH Lopez}

 \author[kaust]{R Tempone}


 \address[kaust]{King Abdullah University of Science and Technology (KAUST), Computer, Electrical and Mathematical Science and Engineering Division (CEMSE), Thuwal, 23955-6900, Saudi Arabia}


\end{frontmatter}

%\linenumbers

We want to thank the anonymous reviewers for providing us with constructive comments and suggestions that significantly improve the quality of the manuscript. In the revised version of the manuscript, you will find modifications that address the concerns of the reviewers.

We want to stress again that the main contribution of this work lies on the derivation of the gradients for two estimators of the Shannon expected information gain, that is, the gradient of the Monte Carlo Laplace approximation and the gradient of the double loop Monte Carlo. Moreover, we successfully tailored recent ideas of Nesterov's accelerated optimizers with the restart technique proposed for deterministic optimization by O'Donoghue and Candes \cite{o2015adaptive} in the stochastic gradient framework. Our approach which differs from the one of Nitanda\cite{nitanda2016accelerated} where variance reduction and mini-batches are used to turn the estimation of the gradient nearly-deterministic. Finally, we provide several engineering numerical examples to highlight our methods.

\section{Reviewer \#1}

Revisions required. The authors have made substantial revisions to their manuscript and addressed most of my earlier comments. I have a few remaining comments:

\begin{itemize}
\item (Referee) Responding to my earlier comment \#7, on the fact that all estimators of expected information gain considered here are *biased* at finite M, the authors propose to evaluate the ``full gradient of the DCMIS estimator'' at the optimum produced by another (cheaper) method, and to use smallness of the gradient norm as an indicator that the biased optimum is not far from the true optimum. This idea of course rests on the DCMIS estimator itself not being strongly biased, and on the bias not varying sharply as a function of the design parameter $\xi$. Also, if the objective is shallow in general, the optimal designs might still be rather far from each other. Overall, the statement seems rather vague. Given its importance, can it be formalized and made more precise? In other words, can you explain under what conditions (on the Bayesian model) this test is meaningful?

\item (Authors) {\blue{When the optimization was carried out using the Laplace estimator for the gradient, that is, SG-LA, we also computed the norm of the gradient using DLMCIS using a large number of inner and outer samples as a sanity check. The norm of this gradient was $10^{-5}$ in general. One has to bear in mind that this is a `sanity check' to double check the effect of the bias in the Laplace approximation and is not meant to be used as an algorithmic tool. Additionally, we agree with the referee: if the objective function is flat, the optimum might be far. However, this also can happen in the deterministic optimization, and the only solution to this problem is to use a smaller tolerance in the stopping criterion.}}

\item (Referee) The authors now state that one of the main contributions of this paper is to derive the gradient of the double-loop Monte Carlo estimator of the expected Shannon information gain. This result is indeed useful, but reference [3] seems to provide a similar derivation (for DLMC, not MCLA).

\item (Authors) {\blue{In reference [3], a surrogate model for the expected information gain is used. Thus, the gradient of this surrogate function is computed. We, instead, derive and present the explicit form of the gradient of the expected information gain for both estimators DLMC and MCLA, both representing a contribution to the field.}}

\item (Referee) One final comment is that the English language usage in the paper is still poor and occasionally hard to understand. The paper would benefit from careful copy-editing by a fluent native speaker.

\item (Authors) {\blue{We apologize for the careless language usage in our first revision. The current version of our manuscript was reviewed by a fluent native speaker at the Publication Services and Researcher Support at our institution KAUST.}}
\end{itemize}

\section{Reviewer \#2}

\begin{itemize}

\item (Referee) I believe that Remark 1 (p.6) is wrong and $\nabla_\xi p(Y(\xi,\theta,\epsilon)|\theta,\xi)$ is not $0$ (in general).

To repeat: We want to compute

\begin{align*}
  \nabla_{\bs{\xi}} \mathbb{E}_{\bs{\theta},\bs{Y}} [f(\bs{\xi}, \bs{\theta}, \bs{Y})] &= \nabla_{\bs{\xi}} \int_\Theta \int_{\mathcal{Y}} f(\bs{\xi}, \bs{\theta}, \bs{Y}) p(\bs{Y}|\bs{\theta},\bs{\xi}\text{d}\bs{Y} \pi(\bs{\theta}) \text{d} \bs{\theta} \\
  &= \int_\Theta \int_{\mathcal{Y}} [\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{Y})] p(\bs{Y}|\bs{\theta},\bs{\xi})\text{d}\bs{Y} \pi(\bs{\theta}) \text{d} \bs{\theta} \\
  & \quad + \int_\Theta \int_{\mathcal{Y}} f(\bs{\xi}, \bs{\theta}, \bs{Y})[\nabla_{\bs{\xi}} p(\bs{Y}|\bs{\theta},\bs{\xi})]\text{d}\bs{Y} \pi(\bs{\theta}) \text{d} \bs{\theta} \\
  &= \int_\Theta \int_{\mathcal{Y}} [\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{Y})] p(\bs{Y}|\bs{\theta},\bs{\xi})\text{d}\bs{Y} \pi(\bs{\theta}) \text{d} \bs{\theta} \\
  & \quad + \int_\Theta \int_{\mathcal{Y}} f(\bs{\xi}, \bs{\theta}, \bs{Y}) k(\bs{Y}|\bs{\theta},\bs{\xi})\text{d}\bs{Y} \pi(\bs{\theta}) \text{d} \bs{\theta},
\end{align*}
where
$$p(\bs{Y}|\bs{\theta},\bs{\xi}) = (2 \pi)^{-q/2} | \bs{\Sigma}_{\bs{\epsilon}}|^{-1/2} \exp{\left[-\frac{1}{2} (\bs{Y} - \bs{g}(\bs{\xi},\bs{\theta}))^T {\bs{\Sigma}_{\bs{\epsilon}}}^{-1} (\bs{Y} - \bs{g}(\bs{\xi},\bs{\theta})) \right]}$$
(for notational convenience, I assume that $N_e=1$) and
\begin{align*}
  k(\bs{Y}|\bs{\theta},\bs{\xi}) &= \nabla_{\bs{\xi}}p(\bs{Y}|\bs{\theta},\bs{\xi})\\
  &= p(\bs{Y}|\bs{\theta},\bs{\xi}) \cdot \nabla_{\bs{\xi}} \bs{g}(\bs{\xi},\bs{\theta})^T {\bs{\Sigma}_{\bs{\epsilon}}}^{-1} (\bs{Y} - \bs{g}(\bs{\xi},\bs{\theta}))
\end{align*}
To show that the second term in this sum vanishes, the authors make the substitution $\bs{Y} = \bs{g}(\bs{\xi},\bs{\theta})+\bs{\epsilon}$. But then the second integral becomes
\begin{align*}
  ... &= \int_{\Theta} \int_{\mathcal{E}} f(\bs{\xi},\bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta})+\bs{\epsilon}) k (\bs{g}(\bs{\xi},\bs{\theta})+\bs{\epsilon} | \bs{\xi}, \bs{\theta}) \text{d}\bs{\epsilon}\pi(\bs{\theta})\text{d}\bs{\theta}\\
  &= \int_{\Theta} \int_{\mathcal{E}} f(\bs{\xi},\bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta})+\bs{\epsilon}) p (\bs{g}(\bs{\xi},\bs{\theta})+\bs{\epsilon} | \bs{\xi}, \bs{\theta}) \cdot \nabla_{\bs{\xi}}\bs{g}(\bs{\xi},\bs{\theta})^T {\bs{\Sigma}_{\bs{\epsilon}}}^{-1} \bs{\epsilon} \text{d}\bs{\epsilon}\pi(\bs{\theta})\text{d}\bs{\theta}\\
  &= \int_{\Theta} \int_{\mathcal{E}} f(\bs{\xi},\bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta})+\bs{\epsilon})
 (2 \pi)^{-q/2} | \bs{\Sigma}_{\bs{\epsilon}}|^{-1/2} \exp{\left[-\frac{1}{2} \bs{\epsilon}^T{\bs{\Sigma}_{\bs{\epsilon}}}^{-1} \bs{\epsilon} \right]} \cdot
 \nabla_{\bs{\xi}}\bs{g}(\bs{\xi},\bs{\theta})^T {\bs{\Sigma}_{\bs{\epsilon}}}^{-1} \bs{\epsilon} \text{d}\bs{\epsilon}\pi(\bs{\theta})\text{d}\bs{\theta}\\
\end{align*}
which is generally not $0$. The mistake in Remark 1 is that the substitution $\bs{Y} = \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon}$ is made BEFORE  taking the derivatives of $p(\bs{Y}|\bs{\theta},\bs{\xi})$ with respect to $\bs{\xi}$, which leads to the wrong conclusion.

The only way to ensure that the second integral is $0$ is to make the substitution before interchanging differentiation and integration, i.e., to compute
\begin{align*}
 \nabla_{\bs{\xi}} \mathbb{E}_{\bs{\theta},\bs{Y}} [f(\bs{\xi}, \bs{\theta}, \bs{Y})] &= \nabla_{\bs{\xi}} \mathbb{E}_{\bs{\theta},\bs{Y}} [f(\bs{\xi}, \bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon})]\\
 &= \nabla_{\bs{\xi}} \int_\Theta \int_{\mathcal{E}} f(\bs{\xi}, \bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon}) p(\bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon}|\bs{\theta},\bs{\xi}\text{d}\bs{\epsilon} \pi(\bs{\theta}) \text{d} \bs{\theta} \\
&= \int_\Theta \int_{\mathcal{E}} [\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon})] p(\bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon}|\bs{\theta},\bs{\xi})\text{d}\bs{\epsilon} \pi(\bs{\theta}) \text{d} \bs{\theta} \\
& \quad + \int_\Theta \int_{\mathcal{E}} f(\bs{\xi}, \bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon})[\nabla_{\bs{\xi}} p(\bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon}|\bs{\theta},\bs{\xi})]\text{d}\bs{\epsilon} \pi(\bs{\theta}) \text{d} \bs{\theta} \\
&= \int_\Theta \int_{\mathcal{E}} [\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon})] p(\bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon}|\bs{\theta},\bs{\xi})\text{d}\bs{\epsilon} \pi(\bs{\theta}) \text{d} \bs{\theta} + \bs{0}
\end{align*}
as I suggested in my previous review. But then one has to be careful, because $\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{Y})$ is not equal to $\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon})$, as I demonstrated in my previous review.
I also shows that the error from using $\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{Y})$ instead of $\nabla_{\bs{\xi}} f(\bs{\xi}, \bs{\theta}, \bs{g}(\bs{\xi},\bs{\theta}) + \bs{\epsilon})$ is likely to be small. Given that the estimators of the expected Shannon information gain are all biased, this additional error may have a negligible effect. However, in my opinion the exposition of the authors is wrong. Since it would be quite easy to correct for this error, I encourage the authors to modify their gradient estimators accordingly. At least they have to correct their exposition, and if they do not change their gradient estimators, they have to acknowledge this additional error, providing a similar analysis to the one in my previous review to justify why the error is likely to be small. However, I believe adapting the gradient estimators would be the easier (and sounder) alternative.

\item (Authors) {\blue{Regarding the point of the referee saying that our derivation of $\nabla_{\bs{\xi}} I(\bs{\xi})$ is wrong, we respectfully disagree. We must assume beforehand that the synthetic data $\boldsymbol{Y}$ depends on $\boldsymbol{\xi}$ to compute the gradient, thus, we arrive at $\nabla_{\bs{\xi}} p(\bs{Y}(\bs{\xi}, \bs{\theta}, \bs{\epsilon}) \vert \bs{\theta}, \bs{\xi})=\bs{0}$.
In our opinion, the derivation presented by the referee is not correct because in computing the gradient w.r.t. $\boldsymbol{\xi}$, he does not consider that $\boldsymbol{Y}$ depends on $\boldsymbol{\xi}$. However, later he replaces $\boldsymbol{Y}$ by its definition which indeed expresses the explicit dependency on $\boldsymbol{\xi}$. We have double checked our derivation and asked some colleagues in the field to check it as well. We all agree that our remark 1 is correct and precise. We kindly ask the referee to reconsider this statement.}}

\item (Referee) Abstract: it is either `the Shannon expected information gain' or `Shannon's expected information gain'; the same applies to `the Nesterov's accelerated gradient descent algorithm' on p. 13 and `the Nesterov's acceleration' in the conclusion.

\item (Authors) {\blue{The corrections proposed by the referee were made.}}

\item (Referee) The formulation `within the stochastic gradient spirit', which is used several times (highlights, abstract, introduction), is a bit odd in my opinion. It is not clear to me what it means. Maybe the authors have something in mind like `within a stochastic gradient (optimization) framework'. In some cases it may be better to completely reformulate the sentences. In the abstract, for example, I think this clause could simply be omitted.

\item (Authors) {\blue{We agree that the expression is not very common. We changed it in all of its occurrences to avoid misunderstandings.}}

\item (Referee) Highlights: I would add `maximizing the expected Shannon information gain' to the first sentence. The second and third sentences are not very clear. I would write something like `We derive the gradient estimator corresponding to the estimator of the expected Shannon information gain based on the Laplace approximation of the posterior.' and `We derive the gradient estimator corresponding to the double loop Monte Carlo estimator of the expected Shannon information gain using Laplace-based importance sampling.'

\item (Authors) {\blue{We agree with the referee that the highlights could be more explicit about the actual content of the paper. However, due to restrictions from Elsevier, we can not have highlights larger than 85 characters. Thus, we could not completely follow the referee's suggestions.}}

\item (Referee) Stylistic suggestion: the word `couple(d)' is used very often. I would replace at least some of the occurrences with synonyms like `combine(d)', `in conjunction with', etc.

\item (Authors) {\blue{We agree that the word ``couple'' was overused. We replaced most of its occurrences with synonyms.}}

\item (Referee) Abstract: What is `a (double loop) Monte Carlo'? Either omit the `a' or add the word `estimator'. Also omit the `a' in `a Laplace-based Importance Sampling'. My suggestion: `a double loop Monte Carlo estimator (DLMC), a Monte Carlo estimator using the/a Laplace approximation for the posterior distribution (MCLA), and a double loop Monte Carlo estimator with Laplace-based importance sampling (DLMCIS).'

\item (Authors) {\blue{We agree that it is important to make clear that we are talking about the estimators. We rewrote the sentence to make this clear. Also, we reviewed this issue in the whole paper.}}

\item (Referee) P.2, last paragraph: I would reformulate the second sentence, e.g. `\ldots in combination with the steepest descent method. We employ three variants: standard stochastic gradient descent (SCD), SCD with Nesterov's acceleration (ASGD), and ASGD with a restart technique (ASGD-restart).'

\item (Authors) {\blue{We agree that the sentence was too long and not very clear. Therefore, we changed it to:}}

{\blue{``To alleviate the computational burden, we use the stochastic gradient (SG), a noisy estimator of the true gradient. We employ three variants: the stochastic gradient descent (SGD), SGD with Nesterov's acceleration (ASGD), and ASGD with a restart technique (ASGD-restart).''}}

\item (Referee) P.5, top: The new sentence about Monte Carlo integration is not at the right place. As it reads now, the marginal likelihood is used for Monte Carlo integration (???). The evidence is equal to the marginal likelihood. After stating the definition of the integral for the marginal likelihood/evidence, it would make sense to mention that it can be estimated using Monte Carlo integration.

One should better stress that the $\bs{Y}$ plugged into the integral within the logarithm in (5) is given by the value of $\bs{Y}$ from the outer integral.
That is, $\bs{Y}$ is generated from $\bs{Y} = g(\bs{\xi}, \bs{\theta}) + \bs{\epsilon}$, where $\bs{\theta}$ is a parameter sample from the outer integral (maybe repeat that relationship to make it clearer), and $\bs{\theta}$ is different from $\bs{\theta}^*$.
The current statement ‘noting that $\bs{Y}$ depends on $\bs{\xi}$, $\bs{\theta}$, and $\bs{\epsilon}$’ is not explicit enough in my opinion.
I think the most important part is to stress is that the parameter $\bs{\theta}$ used to generate $\bs{Y}$ is different from $\bs{\theta}^*$ in the integral within the logarithm.

\item (Authors) {\blue{We agree that the text could be improved and that there could be some misunderstandings concerning $\bs{Y}$ and $\bs{\theta}^*$ dependency. Thus, we removed the sentence about Monte Carlo integration, given that this is done explicitly in Section 3.1, and also included the following sentence in Section 2.2:}}

{\blue{``Bear in mind that $\bs{\theta}^*$ and $\bs{\theta}$ are independent and that $\bs{Y}$ depends on $\bs{\theta}$, $\bs{\xi}$, and $\bs{\epsilon}$, i.e., the parameter $\bs{\theta}$ used to generate $\bs{Y}$ is different from $\bs{\theta}^*$ in the integral within the logarithm.''}}

{\blue{and in Section 3.1:}}

{\blue{``Note that the data $\bs{Y}_n$ are evaluated at $\bs{\theta}_n$.''}}

\item (Referee) Section 3: is it really necessary to show the orders of computational expenditure for the DLMC and MCLA estimators as depending on $h^{-\rho}$? Since none of the examples is concerned with different model evaluation accuracies/mesh sizes h, this could be omitted for simplicity.

\item (Authors) {\blue{In our opinion, even though the mesh is kept constant, we should be consistent and present the bias and computational expenditures in a formal manner.}}

\item (Referee) I do not think the first sentence of Section 3.2 is entirely correct. As I understand it, to obtain the Laplace approximation to the posterior, the logarithm of the posterior is expanded by a second-order (not first-order) Taylor expansion around the posterior mode. Doing that, one arrives at an approximate Gaussian distribution for the posterior. The first sentence in Section 3.2 is a bit unfortunate, as the logic for the derivation of the Laplace approximation is somewhat reversed (starting with the Gaussian approximation before mentioning the Taylor expansion).

Some of the explanations in Section 3.2 would actually better fit into Section 2.2, where the Laplace posterior approximation is introduced. One could then shorten Section 3.2 by referring back to (11).

I would also suggest to move the formulas for the bias and variance ((21) and (22)) a few sentences forward before discussing the precision and bias of the Laplace approximation in the text. This way, one can immediately see the dependence of the bias on Ne and see the order of the bias. In addition, vague concepts like `posterior concentration' can be instantaneously linked to this parameter and the meaning of a term like ‘posterior concentration’ becomes more concrete. Furthermore, it is also immediately evident that the Laplace approximation is not consistent as $N\rightarrow\infty$.

\item (Authors) {\blue{We agree with the referee. We changed the this sentence to}}

{\blue{``The Laplace estimator for the $D_{kl}$ is proposed by Long et al. [2] and relies on approximating the logarithm of the posterior pdf by a second-order Taylor expansion at the maximum posterior estimate. As a consequence, the approximated posterior is Gaussian-distributed.''}}

{\blue{and moved it to Section 2.2}}

{\blue{We reorganized Section 3.2 as suggested by the referee: we now present the bias and variance earlier and included links in the text.}}

\item (Referee) In Section 3.3, the new text passages could be integrated better into the existing parts. After the DLMCIS estimator is described in words in the middle of the first paragraph, it is stated that it is more expensive than MCLA but has less bias. Then the description of the DLMCIS estimator is continued by providing the formulas. It would be better to first provide the full description (including formulas), trying to better integrate the new into the old parts, and then discuss the properties of the DLMCIS estimator and compare it to the other estimators.

\item (Authors) {\blue{We agree that Section 3.3 could be restructured. We reorganized the paragraphs in Section 3.3 as proposed by the referee.}}

\item (Referee) Section 4, paragraph after Equation (24):

The gradient of f is an unbiased estimator for $\nabla_\xi I(\xi)$, not vice versa. I would denote the stochastic gradient estimators by $\mathcal{G} = \widehat{\mathbb{E}_{\bs{\theta},\bs{Y}}[\nabla f]}$ instead $\mathcal{G} \approx
\mathbb{E}_{\bs{\theta},\bs{Y}}[\nabla f ]$to stress that they are estimators and not some arbitrary approximations,
but this is a matter of taste. (Actually it would have to be $\mathcal{G} \approx \widehat{\mathbb{E}_{\bs{\theta},\bs{\epsilon}} [\nabla f ]}$, see
comment 1, the whole section would have to be adapted).
It would also be good to mention here already that the stochastic gradient estimates
are obtained by setting the outer loop sample size to N = 1.

\item (Authors) {\blue{We agree that the mentioned sentence was wrong and fixed it. We also mention that N=1.:

``We name the stochastic gradient estimators of the expected information gain $\mathcal{G} \approx \mathbb{E}_{\bs{\theta}, \bs{Y}} [\nabla f]$, where only one outer sample $N=1$ is used.''}}.

\item (Referee) Section 5.1, p. 12: I do not quite understand the following sentence: `Rather than directly estimating (13) using a large outer loop sample, we consider the gradient of the estimator of I in the optimization process using only one outer sample per iteration, i.e., $N = 1$, equivalently to the SGD approach.' What does `equivalently to the SGD approach' mean? I thought this whole subsection is about the SGD approach?

\item (Authors) {\blue{We agree with the referee and removed the sentence ``,equivalently to the SGD approach''.}}

\item (Referee) P.14, Equation (42): I suppose the phrase ‘is not satisfied’ after (42) should be removed. That is, the optimizer should be restarted when the inner product is $< 0$. Alternatively, one could switch the inequality sign.

\item (Authors) {\blue{We agree with the referee and removed the sentence.}}

\item (Referee) The last sentence of Section 5.3 (p. 15) is incomplete.

\item (Authors) {\blue{We connected it to the last sentence.}}

\item (Referee) P.16, paragraph below Fig. 2: The convergence rate of which method/variance setting is deteriorated compared to which method/variance setting? One could also include a reference to Fig. 1 in this paragraph, especially since mini-batch sampling is mentioned.

\item (Authors) {\blue{We rewrote the phrase as:}}

{\blue{``When the variance $\sigma_{\theta}$ is increased, the convergence deteriorates as well as the error distances $||\bar{\bs{\xi}}_{k}-\bs{\xi}^*||_2$.''}}

{\blue{and included a reference to Fig. 1 where mini-batch sampling is mentioned.}}

\item (Referee) How large is $N_e$ in example 2? Is it $N_e = 1$? This should be mentioned explicitly.

\item (Authors) {\blue{We included this information in the paper, $N_e = 1$.}}

\item (Referee) It should be mentioned in Example 2 how the FGD optimization is actually implemented (large outer loop sample size). This is not clear at the moment. The connection between FGD and the sample sizes mentioned at the bottom of p. 17 is not made explicitly.

\item (Authors) \blue{We moved the sentence mentioning the sample-sizes forward, so that it becomes clear what were the sample-sizes for each case:
``To achieve the tolerance of $0.01$ in the FGD, the optimal numbers of MC samples are  $N^*_{_{\hbox{\tiny{DLMC}}}}=2447$ and $M^*_{_{\hbox{\tiny{DLMC}}}}=80$ for DLMC, $N^*_{_{\hbox{\tiny{DLMCIS}}}}=2402$ and $M^*_{_{\hbox{\tiny{DLMCIS}}}}=7$ for DLMCIS, and $N^*_{_{\hbox{\tiny{MCLA}}}}=966$ for MCLA. We use the same values for their SG estimators, except that $N=1$ is used.''}

\item (Referee) Last paragraph of Section 6.2 (p. 19 + 20): do I understand it correctly that the optimal solution found using SG-LA was assessed using the stochastic gradient of the DLMC estimator with an outer sample size of $10^5$ to estimate the gradient norm? If that is not true, what is the sample that is referred to here with size $10^5$? What is the inner sample size?

What is the tolerance in this example to conclude that a gradient norm of $10^{-6}$ is sufficiently small to be considered close to the optimum? I have the same question for Example 3 (p. 23).
Maybe it would be interesting to mention earlier (say, before Section 6.2.2) how the optimal designs found are assessed.

\item (Authors)

\blue{The gradient was assessed using the DLMCIS estimator. It was observed that the gradient of the DLMCIS estimator converged to a null vector as $N$ and $M$ grow. The MCLA with $N=10^5$ yields a gradient of norm $10^{-6}$. We defined a tolerance on the step performed $\left\| \bs{\xi}_k - \bs{\xi}_{k-1}\right\|_2$ of 0.01 as stopping criterion. Considering $\bs{\xi}_k - \bs{\xi}_{k-1} = \alpha \mathcal{G}_{k-1}$, with the step-size $\alpha=1.0$, the one used in at the start of the optimization, a gradient with norm $\left\| \mathcal{G}\right\|_2 = 10^{-6}$
results in a step performed with norm $\left\| \bs{\xi}_k - \bs{\xi}_{k-1} \right\|_2 = 10^{-6}$, four orders of magnitude smaller than the stopping criterion. Thus, for the desired precision in the optimization, the bias of the Laplace estimation is not significant.}

The computation of the norm of the gradient is a sanity check to assess the bias. To be more clear, we rewrote the following paragraph in \blue{example 2:

``As a sanity check, to estimate the intrinsic bias of the Laplace approximation in the optimization carried out with the estimator SG-LA, we computed the expected value of gradient using DLMCIS at the optimum found. Using $N=10^5$ and $M=10^3$ in DLMCIS, we obtained a gradient with a norm of $10^{-6}$, which means that the bias introduced by the Laplace approximation is negligible in this case.''}

\blue{and in example 3:

``Since we used the biased and inconsistent SG-LA estimator of the gradient, as a sanity check, we evaluated the gradient at the optima we found (the first two cases) using the full gradient of the DLMCIS estimator with $N=10^3$ and $M=10^2$. In both cases, the gradient norm was below $10^{-3}$, meaning that the bias of the Laplace approximation is considerably small at the optima.''.}

\item (Referee) It should be mentioned explicitly for all examples how the optimal $\boldsymbol{\xi}^*$ depicted in Figures 2, 4, 5, 8, and 14 was found, similar to the authors' answer to my question 5 from the previous review.

\item (Authors) {\blue{In 6.1 it was included ``The optimum of $I$ has a closed form $\xi^*_i = -\sigma_\theta^2$.''}}

{\blue{and in 6.3 ``To plot the convergence, we estimated the real optima using DLMCIS with FGD from the optima found using the SG-LA.''.}}

{\blue{In 6.4, we present the self-convergence test as it is usual in numerical analysis.}}

\item (Referee) Figure 9: As I understand it, the Laplace approximation (7) was used to draw the posterior contours. However, which $\hat{\boldsymbol{\theta}}$ was used? It seems like $\hat{\boldsymbol{\theta}}$ was set to the prior means. Please mention that in the paper (e.g. in the figure caption).

\item (Authors) {\blue{The contours were evaluated using the the expected value of $\theta$ according to the prior distribution. The following was added to Section 6.3: ``The posteriors are evaluated at $\bs{\hat{\theta}} = (\mu_{pr}^E,\mu_{pr}^G)$ for the four cases are presented in Figure 9.''}}

{\blue{and to Section 6.4.2: ``We present in Figure 11 the electric potential and the current streamlines both before and after the optimization.
We also present the expected information gain when using the MCLA estimator with the optimization path and the posteriors evaluated at $\bs{\hat{\theta}} = (\frac{\pi}{3.9375},-\frac{\pi}{3.9375})$.''}}

\item (Referee) Example 4, Section 6.4.1, p. 25: I repeat my previous question 33. Everything is now properly defined, also $H^1$, so this is only of minor importance. However, I am still wondering if it would be possible to reorganize this subsection so that $L^2_{\mathbb{P}}$ and $\boldsymbol{\jmath}$ do not appear BEFORE they are defined.

\blue{\item (Authors) We reorganized the text so that $\boldsymbol{\jmath}$ and $\boldsymbol{I}_e$ are introduced as they are used. $L^2_{\mathbb{P}}$, however, needs to be defined later.}

\item (Referee) Example 4, Section 6.4.2: in (55), the posterior covariances at the initial guess and the optimum solution are computed using the Laplace approximation. Similar to question 21, what value was assumed for $\hat{\boldsymbol{\theta}}$? Furthermore, how many repeated experiments, $N_e$, were assumed for this example (relevant for the bias of the Laplace approximation)?

\blue{\item (Authors) We computed the covariances at the means of the priors of $\bs{\theta}$. Thus, we included the following in Section 6.4.2:

``We also present the expected information gain when using the MCLA estimator with the optimization path and the posteriors evaluated at $\bs{\hat{\theta}} = (\frac{\pi}{3.9375},-\frac{\pi}{3.9375})$. ''

The number of experiments was presented in the end of Section 6.4.1, but we moved it to the start of Section 6.4.2 as

``In all cases the number of experiments is $N_e = 1$.''}

\item (Referee) This question extends question 34 of my previous review. I still do not understand the sentence on p. 28: `Both optimization searches converge to a setup where one electrode has zero value, and the contours show that electrodes with a non-zero value must be on different sides of the plate.' If I understand it correctly, the current at the bottom electrode is only 0 if the currents applied to the two upper electrodes sum to 0. With respect to Figure 12, this corresponds to all configurations on the diagonal from the top left to the bottom right. The expected information gain at these points is 0, so these points cannot be optimal. Is that what is meant with `the contours show that electrodes with a non-zero value must be on different sides of the plate'? Please be more specific with `what the contours show'.

\item (Authors) We rewrote the paragraph as

``As can be observed in Figure 12, this problem has four optima: $(0,1)$, $(1,0)$, $(0,-1)$, and $(-1,0)$. These optima have in common that one of the two top electrodes has null-current while the other two electrodes have current 1 or -1.
Figure 12 shows that the optimization converges to local optima for the two initial guesses, arriving at solutions where the expected information gain is around $2.4$.''

\item (Referee) The conclusion is one big paragraph. It should be split up into several paragraphs where appropriate.

\item (Authors) {\blue{We agree with the referee. The concluding remarks have been split up into four paragraphs.}}

\item (Referee) Conclusion: `SG-MCIS also has the advantage of not introducing more bias.' This sentence is a bit too vague. More bias compared to what? SG-MC? Then I would write that SG-MCIS does not introduce a larger bias than SG-MC. Comparing it to SG-LA is more difficult, since the bias of SG-LA depends on Ne and the bias of SG-MCIS depends on the inner loop sample size $M$. However, the bias of SG-MCIS can be reduced by increasing $M$.

\item (Authors) {\blue{We rewrote the sentence to}}

{\blue{``SG-MCIS is more expensive than SG-LA, but less costly than SG-MC. However, SG-MCIS has the advantage of being a consistent estimator, whereas SG-LA is not.
''}}

\end{itemize}

\section*{References}

\bibliography{Response}

\end{document}
