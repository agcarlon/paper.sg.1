\documentclass[3p, 12pt, sort&compress]{elsarticle}
\usepackage{float} % To re-style the 'figure' float
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{graphics}
\usepackage{amsthm}
\usepackage{latexsym,amsfonts}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{xcolor}

\newcommand\myeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\scriptsize def}}}{=}}}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\blue}[1]{{ #1}}

\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\bs}[1]{\boldsymbol{#1}}

\pdfsuppresswarningpagegroup=1
% margin notes
\begin{document}

\begin{frontmatter}



 \title{Response to reviewers\\ {\small Nesterov-aided Stochastic Gradient Methods using Laplace Approximation for Bayesian Design Optimization}}


 % Group authors per affiliation:
 \author[ufsc]{AG Carlon\corref{mycorrespondingauthor}}
 \cortext[mycorrespondingauthor]{Corresponding author.\\
  \quad E-mail addresses: a.g.carlon@posgrad.ufsc.br (AG Carlon),
  ben.dia@kfupm.edu.sa (BM Dia),
  espath@gmail.com (LFR Espath),
  rafaelholdorf@gmail.com (RH Lopez),
 raul.tempone@kaust.edu.sa (R Tempone)}
 \address[ufsc]{Federal University of Santa Catarina (UFSC), Department of Civil Engineering, Rua Jo\~ao Pio Duarte da Silva, Florian\'opolis, SC, 88040-970, Brazil}

 \author[kfupm]{BM Dia}
 \address[kfupm]{ King Fahd University of Petroleum and Minerals (KFUPM), College of Petroleum Engineering and Geosciences, Center for Integrative Petroleum Research (CIPR), Dhahran 31261, Saudi Arabia}

 \author[kaust]{LFR Espath}

 \author[ufsc]{RH Lopez}

 \author[kaust]{R Tempone}


 \address[kaust]{King Abdullah University of Science and Technology (KAUST), Computer, Electrical and Mathematical Science and Engineering Division (CEMSE), Thuwal, 23955-6900, Saudi Arabia}


\end{frontmatter}

%\linenumbers

We want to thank the anonymous reviewers for providing us with suggestions and corrections that greatly improved the quality of the manuscript.
In the revised version of the manuscript, you will find modifications that address the concerns of the reviewers colored in red.

We want to stress again that the main contribution of this work lies on the derivation of the gradients for two estimators of the Shannon expected information gain, that is, the gradient of the Monte Carlo Laplace approximation and the gradient of the double loop Monte Carlo. Moreover, we successfully tailored recent ideas of Nesterov's accelerated optimizers with the restart technique proposed for deterministic optimization by O'Donoghue and Candes \cite{o2015adaptive} in the stochastic gradient framework. Our approach which differs from the one of Nitanda\cite{nitanda2016accelerated} where variance reduction and mini-batches are used to turn the estimation of the gradient nearly-deterministic. Finally, we provide several engineering numerical examples to highlight our methods.

\section{Reviewer}

\begin{itemize}


\item (Referee) 1. In the answer to my comment 1 from the review of the first revision...

\item (Authors) Addressing the reviewer's first question, we agree that the dependency of $\bs{Y}$ on $\bs{\xi}$ results in the extra terms mentioned.
To illustrate to the reader that Proposition 1 is only valid for our assumption of experimental model with additive noise, we included a detailed proof.
We develop the gradient of the expected information gain considering the dependency of $\bs{Y}$ in $\bs{\xi}$, then, similarly to the results presented by the referee, we show that :
\begin{equation} \label{eq:A2}
  \begin{split}
  \nabla_{\bs{\xi}} I(\bs{\xi}) &= \nabla_{\bs{\xi}} \int_{\Theta} \int_{\mathcal{E}}  \log \left( \frac{p(\bs{Y} \vert \bs{\theta}, \bs{\xi})}{p(\bs{Y}\vert \bs{\xi})} \right)  p(\bs{Y} \vert \bs{\theta}, \bs{\xi}) \det (\nabla_{\bs{\epsilon}}\bs{Y}) \text{d}\bs{\epsilon} \pi(\bs{\theta})  \text{d} \bs{\theta}\\
  &= \int_{\Theta} \int_{\mathcal{E}} \nabla_{\bs{\xi}} \log \left( \frac{p(\bs{Y} \vert \bs{\theta}, \bs{\xi})}{p(\bs{Y}\vert \bs{\xi})} \right)  p(\bs{Y} \vert \bs{\theta}, \bs{\xi}) \det (\nabla_{\bs{\epsilon}}\bs{Y}) \text{d}\bs{\epsilon} \pi(\bs{\theta})  \text{d} \bs{\theta}\\
  & \quad + \red{\int_{\Theta} \int_{\mathcal{E}} \log \left( \frac{p(\bs{Y} \vert \bs{\theta}, \bs{\xi})}{p(\bs{Y}\vert \bs{\xi})} \right)  \nabla_{\bs{\xi}}  p(\bs{Y} \vert \bs{\theta}, \bs{\xi}) \det (\nabla_{\bs{\epsilon}}\bs{Y}) \text{d}\bs{\epsilon} \pi(\bs{\theta})  \text{d} \bs{\theta}}\\
  & \quad + \red{\int_{\Theta} \int_{\mathcal{E}} \log \left( \frac{p(\bs{Y} \vert \bs{\theta}, \bs{\xi})}{p(\bs{Y}\vert \bs{\xi})} \right)  p(\bs{Y} \vert \bs{\theta}, \bs{\xi}) \nabla_{\bs{\xi}} \det (\nabla_{\bs{\epsilon}}\bs{Y}) \text{d}\bs{\epsilon} \pi(\bs{\theta})  \text{d} \bs{\theta}}.
\end{split}
\end{equation}
For the additive experimental model we use, the two terms marked in red vanish, resulting in Proposition 1.


\item (Referee) 2. p. 6, first line: $\bs{\Sigma}^{-1}$ is the Hessian matrix of the negative logarithm of the posterior pdf, not the inverse Hessian.

\item (Authors) Indeed, $\bs{\Sigma}^{-1}$ is the Hessian matrix of the negative logarithm of the posterior pdf.
We corrected this mistake in the manuscript.



\item (Referee) 3. Section 3.3: The Laplace posterior $\pi(\bs{\theta}^*) \sim \mathcal{N} (\hat{\bs{\theta}}, \bs{\Sigma}(\bs{\xi}_k),\hat{\bs{\theta}})$ depends on $\hat{\bs{\theta}}$. I assume $\hat{\bs{\theta}}$ is meant to be estimated from Equation (10), as mentioned in Example 2. It would be good to mention also in Section 3.3 what $\hat{\bs{\theta}}$ is and how it can be found.

\item (Authors) To use the importance sampling, the MAP and the approximated covariance matrix must be estimated.
Even though these were previously presented in the text, we included the following in Section 3.3:

`` The change of measure in the importance sampling requires the evaluation of the MAP and the approximated covariance matrix at the MAP.
As can be observed in (10), finding the MAP is an optimization problem on itself.
Due to its robustness and global convergence quality, we employ the Nelder-Mead algorithm \cite{nelder1965simplex} to find the MAP.
As for $\bs{\Sigma}$, it can be calculated from (11) using the Jacobian of the forward model with respect to $\bs{\theta}$.''.



\item (Referee) 4. p. 9, first paragraph: Should it not be $NMh^{-\rho}$ evaluations of the forward model for DLMCIS instead of $(\dim (\bs{\xi})+1)NMh^{-\rho}$?
As I understand it, $(\dim (\bs{\xi})+1)NMh^{-\rho}$ would apply to the gradient estimator when using finite differences.
Furthermore, adding the factor $h^{-\rho}$ gives the cost rather than the number of evaluations.

\item (Authors) Indeed, the cost of the estimator was not right.
The actual cost is $N(\textrm{dim}(\bs{\theta})+1+ M + C_{MAP}) h^{-\varrho}$, where $\textrm{dim}(\bs{\theta})+1$ is the cost of estimating the Jacobian required to calculate $\bs{\Sigma}$ by forward finite differences, and $C_{MAP}$ is the number of of model evaluations required to find $\bs{\hat\theta}$.
To make that clear, we included the following phrase in the manuscript:

``If forward differences are used to approximate the Jacobian of the model with respect to $\bs{\theta}$, needed to approximate $\bs{\Sigma}$, each evaluation of the DLMCIS estimator has cost $N(d+1+ M + C_{MAP}) h^{-\varrho}$, where $C_{MAP}$ is the number of model evaluations required to find $\bs{\hat\theta}$.'',

noting that we substituted $\dim(\bs{\theta})$ by $d$ in the manuscript.
.



\item (Referee) 5.  Section 6.1, p. 16. I supppose Equation (49) and the gradient approximations below should be negative.

\item (Authors) We agree that the equations miss the negative sign.
We fixed the mentioned equations.



\item (Referee) 6. Section 6.1: The value used for $n$ is not mentioned explicitly ($n=20$ according to the eigenvalues of $\bs{A}$).

\item (Authors) Indeed, the value of $n$ used in this numerical example was missing.
We included the value of $n$ explicitly in Section 6.1.



\item (Referee) 7. Section 6.2, p.17: Equations (51) and (52): is it $-4 \bs{1}\theta$ or $-8 \bs{1} \theta$?

\item (Authors) The referee is right. The different value in the two equations was a typographical error and was fixed.



\item (Referee) 8. Section 6.4.1, p.24 first paragraph: Maybe instead of `an orthogonal matrix that rules the rotation of the unknown orientation angle $\theta_k$ of ply $k$' something like `an orthogonal matrix depending on the unkown orientation angle $\theta_k$ which governs the rotation of ply $k$.'

\item (Authors) We believe that the way the referee wrote is clearer. We substituted the original phrase by the suggestion given.



\item (Referee) 9. Section 6.4.2, p. 26: I suppose the values of $\bs{\hat\theta}$ used to evaluate the approximate covariance matrices in Equation (59) are the same values mentioned on p. 27 that were used to draw the posteriors in Fig. 11. It would be good to reorganize this section to know which $\bs{\hat\theta}$ was used to compute the covariance matrices.

\item (Authors) Indeed, we did not make clear that we used the mean of the prior as an approximation of $\bs{\hat\theta}$ in the calculation of $\bs{\Sigma}_{post}$.
We included the following in the start of the subsection ``Numerical tests for EIT'', Section 6.4.2:

``To generate the plots with the posteriors pdfs, the MAP is approximated by the mean of the prior, i.e., $\bs{\hat{\theta}} = (\frac{\pi}{3.9375},-\frac{\pi}{3.9375})$.''.

Then, we included the following before the covariance matrices for test case 1 are presented:

``We approximate the covariance of the posterior pdf for each $\bs{\xi}$ by $\bs{\Sigma}_{post}(\bs{\xi})$, as presented in (11), using the mean of the prior to approximate the MAP.''



\item (Referee) 10. Section 6.4.2, last sentence on p. 29: I do not understand with respect to which vertical axis the two optima are reflections.
Is this referring to the current streamlines graph at the top of Fig. 13, where the streamlines of the two optima are reflections of each other along the vertical line at x = 10?
On the other hand, if this sentence is referring to Fig. 12, would the two optima not be reflections with respect to the $45^\text{o}$ line?

\item (Authors) We agree with the reviewer that the sentence was unclear.
We substituted in to

``This problem is symmetric in the vertical axis, as can be seen in Figure 12.
Because of this symmetry, the two optima found, $(1,0)$, and $(0,1)$ are reflections of one another over the symmetry axis, the reason why the two optimized posteriors look alike.
Moreover, this symmetry results in the diagonal symmetry of the expected information gain that can be observed in Figure 13.''
\end{itemize}

% \section*{References}

\bibliography{Response}

\end{document}
