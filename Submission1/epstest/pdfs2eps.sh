#!/bin/bash
for FILE in ./*.pdf; do
  pdftops -f 1 -l 1 -eps "${FILE}"
  rm  "${FILE}"
done
